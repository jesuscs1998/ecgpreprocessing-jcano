function [ecg_processed]=function_ECGpreprocessing_jcano(selected_ecg,fs)
% Author: Jesús Cano Serrano
%% Remove recording spikes
% Spikes of great amplitude are removed using a threshold of 2000 times the median value of the signal.
% This value is replaced by the median of the signal
% Other types of locators have been tested, such as "filloutliers", but
% detect unwanted peaks

spikes=selected_ecg>2000*median(selected_ecg) | selected_ecg<-2000*median(selected_ecg);
ecg_r=selected_ecg;
ecg_r(spikes)=median(selected_ecg);

%% Low-pass filtering
%Frequency range of the ECG signal is approximately up to 150 Hz
%However, in order to clearly represent QRS complex and T wave, it was decided to reduce the cutoff frequency to 70 Hz.
%Low-pass fourth order Butterworth filter is applied with fc=70 Hz
fc=70;
[B,A] = butter(6,fc/(fs/2));
ecg_lp=filtfilt(B,A,ecg_r);

%%  Powerline interference filtering
% Alternating current oscillates at a frequency of 50 Hz
%Butterworth notch filter with width defined by the 49 to 51 Hz frequency interval.
d = designfilt('bandstopiir','FilterOrder',2, ...
               'HalfPowerFrequency1',49,'HalfPowerFrequency2',51, ...
               'DesignMethod','butter','SampleRate',fs);

ecg_p=filtfilt(d,ecg_lp);

%% Baseline wander and offset removal
%To remove baseline wander, the polynomial trend is removed and baseline is
%established in 0
 ecg_processed=detrend(ecg_p);

end