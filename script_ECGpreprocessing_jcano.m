%% ELECTROCARDIOGRAM SIGNAL PROCESSING TEST
% Author: Jesús Cano Serrano

load('ecgConditioningExample.mat'); %Load ECG signals
%% Plot ECG signals
figure('units','normalized','outerposition',[0 0 1 1])
t = (0:length(ecg)-1)/fs;
for i=1:size(ecg,2)
subplot(size(ecg,2),1,i)
plot(t,ecg(:,i))
ylabel('Amplitude (mV)')
xlabel('Time (s)')
 title(['Channel ', num2str(i), ' raw ECG signal'])
end
%% List channel selection
list = {'Channel 1','Channel 2','Channel 3',...              
'Channel 4','Channel 5','Channel 6'};
[selected_channel,tf] = listdlg('PromptString',{'Only one channel can be selected at a time.',''},...
    'SelectionMode','single','ListString',list);
%% Detect if a channel is not connected
sum_signals=sum(ecg(:,selected_channel));
    if sum_signals==0
        text1=['Channel ', num2str(selected_channel), ' is not connected, run the script again and choose another channel'];
        disp(text1)
    else
        selected_ecg=ecg(:,selected_channel);
        [ecg_processed]=function_ECGpreprocessing_jcano(selected_ecg,fs);
        text1=['Channel ', num2str(selected_channel), ' is connected'];
        disp(text1)
        
        figure('units','normalized','outerposition',[0 0 1 1])       
        subplot(2,1,1)
        plot(selected_ecg)
        ylabel('Amplitude (mV)')
        xlabel('Time (s)')
         title(['Channel ', num2str(selected_channel), ' raw ECG signal'])
        subplot(2,1,2)
        plot(ecg_processed)
        ylabel('Amplitude (mV)')
        xlabel('Time (s)')
         title(['Channel ', num2str(selected_channel), ' processed ECG signal'])
    end